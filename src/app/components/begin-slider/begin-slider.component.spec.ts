import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeginSliderComponent } from './begin-slider.component';

describe('BeginSliderComponent', () => {
  let component: BeginSliderComponent;
  let fixture: ComponentFixture<BeginSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BeginSliderComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeginSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
