import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-begin-slider',
  templateUrl: './begin-slider.component.html',
  styleUrls: ['./begin-slider.component.scss']
})
export class BeginSliderComponent implements OnInit {
  prevAvailable: boolean;
  nextAvailable: boolean;
  slideOpts = {
    initialSlide: 0,
    speed: 400
  };
  @ViewChild('slides') slidesRef: IonSlides;
  constructor() {}

  ngOnInit() {
    this.prevAvailable = false;
    this.nextAvailable = true;
  }
  // check if reached either the start or end slides - needed for when dragging.
  checkSlidesEndStart() {
    this.checkEndSlide();
    this.checkStartSlide();
  }
  // change to next slide
  next() {
    this.slidesRef.slideNext();
    this.checkEndSlide();
  }
  // change to prev slide
  prev() {
    this.slidesRef.slidePrev();
    this.checkStartSlide();
  }
  // check if reached the last slide, and deactivate next button if yes.
  checkEndSlide() {
    this.slidesRef.isEnd().then(result => {
      // disable next button
      this.nextAvailable = !result;
    });
    this.prevAvailable = true;
  }
  // check if reached the first slide, and deactivate prev button if yes.
  checkStartSlide() {
    this.slidesRef.isBeginning().then(result => {
      // disable prev button
      this.prevAvailable = !result;
    });
    this.nextAvailable = true;
  }
}
