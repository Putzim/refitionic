import { Component, OnInit } from '@angular/core';
import { INavLinks } from 'src/app/shared/interfaces/inav-links';
import { navLinksList } from './navigation-links';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
})
export class WelcomeComponent implements OnInit {
  navLinks: INavLinks[] = navLinksList;
  linkColor = '#fff';
  coverClasses = {
    cover: true,
  };
  constructor() { }

  ngOnInit() {}

}
