import { INavLinks } from '../../shared/interfaces/inav-links';

export const navLinksList: INavLinks[] = [
  {
    label: 'Home',
    route: ['guest'],
    icon: '',
  },
  {
    label: 'About',
    route: ['about'],
    icon: '',
  },
  {
    label: 'Contact Us',
    route: ['contact-us'],
    icon: '',
  },
  {
    label: 'Log In',
    route: ['log-in'],
    icon: '',
  },
  {
    label: 'Sign Up',
    route: ['sign-up'],
    icon: '',
  },
];
