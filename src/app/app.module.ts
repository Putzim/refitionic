import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule, routedComponents } from './app-routing.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { UserPageModule } from './user/user.module';
import { HomePageModule } from './home/home.module';
import { GlobalModule } from './global/global.module';
import { SharedModule } from './shared/shared.module';
import { AuthService } from './user/services/auth.service';
import { AuthGuardService } from './user/services/auth-guard.service';
import { MailService } from './core/mail.service';
import { SharedService } from './shared/services/shared.service';
import { FooterComponent } from './components/footer/footer.component';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
  }

@NgModule({
  declarations:
  [
    // components , directives , pipes
    AppComponent,
    FooterComponent,
    routedComponents
  ],
  entryComponents: [],
  imports: [
    // --all modules that i used in this module--
    // angular modules
    BrowserModule,
    IonicModule.forRoot(),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
           provide: TranslateLoader,
           useFactory: (createTranslateLoader),
           deps: [HttpClient]
         }
      }),
      // our modules
      HomePageModule,
      UserPageModule,
      GlobalModule,
      SharedModule,
      // put after all modules with routhing because of routing to ** in this one
      AppRoutingModule,
  ],
  providers: [
    AuthService,
    AuthGuardService,
    MailService,
    SharedService,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  // first running  component
  bootstrap: [AppComponent]
})
export class AppModule {}
