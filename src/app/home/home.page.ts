import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { INavLinks } from '../shared/interfaces/inav-links';
import { navLinksList } from './navigation-links';
import { SharedService } from '../shared/services/shared.service';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  navLinks: INavLinks[] = navLinksList;
  linkColor = '#000';
  animationFromLocation = {
    value: 'test', params: { perX: '100', perY: '0' }
  };
// tslint:disable-next-line: variable-name
  constructor(private _sharedService: SharedService) {

  }

  menuClicked() {
    // this._sharedService.menuClicked.emit();
  }

}
