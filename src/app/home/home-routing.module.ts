import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../user/services/auth-guard.service';
import { TermsComponent } from '../global/components/terms/terms.component';
import { ContactUsComponent } from '../global/components/contact-us/contact-us.component';
import { AboutUsComponent } from '../global/components/about-us/about-us.component';
import { HomePage } from './home.page';


const routes: Routes = [
    {
      path: 'home',
      component: HomePage,
      canActivate: [AuthGuardService],
      children: [
        {
          path: '', redirectTo: 'me', pathMatch: 'full'
        },
        {
          path: 'terms',
          component: TermsComponent,
        },
        {
          path: 'contact-us',
          component: ContactUsComponent,
        },
        {
          path: 'about',
          component: AboutUsComponent,
        },
      ]
    }
  ];

@NgModule({
    imports: [
      RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
  })
export class HomeRoutingModule { }
export const routedComponents = [
    HomePage,
  ];
