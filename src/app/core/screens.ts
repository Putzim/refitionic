export const screenSize =  {
    'x-small-upper-boundary': 480,
    'phone-upper-boundary': 600,
    'medium-upper-boundary': 768,
    'tablet-portrait-upper-boundary': 900,
    'tablet-landscape-upper-boundary': 1200,
    'desktop-upper-boundary': 1800
};


