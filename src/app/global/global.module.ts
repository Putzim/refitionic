import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { TermsComponent } from './components/terms/terms.component';

@NgModule({
  declarations: [AboutUsComponent, ContactUsComponent, TermsComponent],
  imports: [
    CommonModule
  ]
})
export class GlobalModule { }
