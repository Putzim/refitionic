import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LoginComponent } from './user/components/login/login.component';
import { SignUpComponent } from './user/components/sign-up/sign-up.component';
import { AboutUsComponent } from './global/components/about-us/about-us.component';
import { ContactUsComponent } from './global/components/contact-us/contact-us.component';
import { TermsComponent } from './global/components/terms/terms.component';
import { AuthGuardService } from './user/services/auth-guard.service';
import { HomePageModule } from './home/home.module';
import { CoachPageModule } from './coach/coach.module';
import { BeginSliderComponent } from './components/begin-slider/begin-slider.component';

const routes: Routes = [
  {
    // default path
    path: '', redirectTo: 'welcome', pathMatch: 'full'
  },
  {
    path: 'welcome',
    component: WelcomeComponent,
    children: [
      {
        path: '', redirectTo: 'guest', pathMatch: 'full'
      },
      {
        path: 'guest',
        component: BeginSliderComponent,
      },
      {
        path: 'log-in',
        component: LoginComponent,
      },
      {
        path: 'sign-up',
        component: SignUpComponent,
      },
      {
        path: 'about',
        component: AboutUsComponent,
      },
      {
        path: 'contact-us',
        component: ContactUsComponent,
      },
      {
        path: 'terms',
        component: TermsComponent,
      },
    ]
  },
  {
    path: 'home',
    canActivate: [AuthGuardService],
    loadChildren: () => HomePageModule, // lazy load
    data: { preload: true }  // preload in background to be ready to use when user navigates to home
  },
  {
    path: 'coach',
    canLoad: [AuthGuardService],
    // component: AdminComponent,
    // lazy load with preload is the best configuration
    loadChildren: () => CoachPageModule, // lazy load
    data: { preload: true }  // preload in background to be ready to use when user navigates to home
  },
  {
    // not legal path
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routedComponents = [
  WelcomeComponent,
  PageNotFoundComponent,
  BeginSliderComponent
];
