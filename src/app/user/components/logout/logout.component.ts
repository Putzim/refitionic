import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'user-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss'],
})
export class LogoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

}
