import { Component, OnInit, HostListener, Input } from '@angular/core';
import { screenSize } from 'src/app/core/screens';
import { Router } from '@angular/router';
import { INavLinks } from '../../interfaces/inav-links';

@Component({
  selector: 'sh-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {

  showNav = true;  // on small screen nav is replaced by burger
  showBurger = false;
  @Input() constantNav = false; // define if nav is always present (for home page)
  activeLinkIndex = 0;
  tabsClass: any;
  innerWidth: number;       // width of the screen. used to set showNav
  @Input() navLinks: INavLinks[];      // links array
  // default link color is black
  @Input() linkColor = '#000'; // link color comes from the parent (differs on various screens)
  //////////////////////////////////////////////////////////////////////////////////
  // this event happens when screen is resized
  //////////////////////////////////////////////////////////////////////////////////
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    this.setShowNav();
  }
// tslint:disable-next-line: variable-name
  constructor(private _router: Router) { }
  //////////////////////////////////////////////////////////////////////////////////
  // set navigation according to the screen size
  //////////////////////////////////////////////////////////////////////////////////
  ngOnInit() {
    this.innerWidth = window.innerWidth;
    this.setShowNav();
    this.setActiveClass(0);
    // fix active link bug - this solution doesn't work
  }
  //////////////////////////////////////////////////////////////////////////////////
  // the following functions solves angular material tab bug,
  // which doesn't set properly the active route class
  //////////////////////////////////////////////////////////////////////////////////
  setActiveClass(indexOfRouteLink) {
    this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.route[0] === this._router.url.split('/').pop()));
    this.tabsClass =  {'mat-tab-label-active': 'false'};
    if (this.activeLinkIndex === indexOfRouteLink) {
      this.tabsClass = {'mat-tab-label-active': 'true'};
    }
    return this.tabsClass;
  }
  //////////////////////////////////////////////////////////////////////////////////
  // choose between full nav and burger nav on small screens
  //////////////////////////////////////////////////////////////////////////////////
  setShowNav(): void {
    if (this.innerWidth < screenSize['medium-upper-boundary']) {
      this.showNav = this.constantNav; // false if not constant
      this.showBurger = !this.constantNav; // true if not constant main nav
    } else {
      this.showNav = true;
      this.showBurger = false;
    }
  }
}
