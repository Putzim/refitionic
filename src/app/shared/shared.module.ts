import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './components/nav/nav.component';
import { LogoComponent } from './components/logo/logo.component';
import { IonicModule } from '@ionic/angular';
import { HomeNavComponent } from './components/home-nav/home-nav.component';

@NgModule({
  declarations: [NavComponent, LogoComponent, HomeNavComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    NavComponent,
    LogoComponent,
    HomeNavComponent
  ]
})
export class SharedModule { }
